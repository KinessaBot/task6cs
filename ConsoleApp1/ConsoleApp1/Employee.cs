﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Employee : IComparable
    {
        public enum CompareMods
        {
            BySalary,
            ByInfo
        }
        public string personalInfo { get; set; }
        public decimal salary { get; set; }

        public Employee(string personalInfo, decimal salary)
        {
            this.personalInfo = personalInfo;
            this.salary = salary;
        }

        public int CompareTo(object o, CompareMods mod)
        {
            if(mod==CompareMods.BySalary)
                return this.salary.CompareTo(((Employee)o).salary);
            return this.personalInfo.CompareTo(((Employee)o).personalInfo);
        }

        public static int CompareByInfo(Employee o1, Employee o2)
        {
            return o1.personalInfo.CompareTo(o2.personalInfo);
        }

        public int CompareTo(object o)
        {
            return this.salary.CompareTo(((Employee)o).salary);
        }

        public static void Print(Employee employee)
        {
            Console.WriteLine(employee.ToString());
        }
        public override string ToString()
        {
            return this.personalInfo + " with salary - " + this.salary;
        }
    }
}
