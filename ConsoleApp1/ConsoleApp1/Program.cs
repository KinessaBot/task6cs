﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> list = new List<Employee>();
            list.Add(new Employee("Ivanov", 20M));
            list.Add(new Employee("Petrov", 40M));
            list.Add(new Employee("Sidorov", 10M));
            list.Add(new Employee("Kravcev", 30M));
            list.ForEach(Employee.Print);  // start list
            Console.WriteLine("_________");

            list.Sort();            // sort with IComparable(default comparer)
            list.ForEach(Employee.Print);
            Console.WriteLine("_________");

            list.Sort(Employee.CompareByInfo); // sort with Comparer
            list.ForEach(Employee.Print);
            Console.WriteLine("_________");

            list.Sort((o1, o2) => o1.CompareTo(o2, Employee.CompareMods.ByInfo)); // sort with lambda
            list.ForEach(Employee.Print);
            Console.WriteLine("_________");

            list.Sort(delegate (Employee employee1, Employee employee2)  //sort with Comparison
            {
                return employee1.CompareTo(employee2);
            }
            );
            list.ForEach(Employee.Print);
            Console.WriteLine("_________");
            Console.ReadLine();
        }
    }
}
